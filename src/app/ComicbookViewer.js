import React from 'react';
import PropTypes from 'prop-types';
import ComicbookImage from "./ComicbookImage";
import {Cookies, withCookies} from "react-cookie";

const URL = `http://hedgalex.nz:8001/comic`;
const defaultPosition = 2;

class ComicbookViewer extends React.Component {

    constructor(props) {
        super(props);

        this.dragX = 0;
        this.animation = false;
        this.imageWidth = 0;
        this.imageWidthInPercents = 100 / props.images.length;
        let {chapter, page} = props.match.params;

        if(chapter && chapter.match(/^\d+/) && chapter !== '0' && page && page.match(/^\d+/) && page !== '0') {
            this.state = {
                position    : defaultPosition,
                seeker      : 0,
                pages       : [],
                animate     : false
            };
            this.onDragStart = this.onDragStart.bind(this);
            this.onDragEnd = this.onDragEnd.bind(this);
            this.onDrag = this.onDrag.bind(this);
            this.onTouch = this.onTouch.bind(this);

            document.addEventListener("keydown", this._handleKeyDown.bind(this));
        } else {
            this.state = {error: 404}
        }

    }

    componentWillMount() {
        let name = this.props.match.params.name;
        fetch(`${URL}/details/${name}`)
            .then(results => {
                return results.json();
            })
            .then(data => {
                this._distributeImages(data, this.props)
            })
    }

    componentDidMount() {
        // '.comicbook-viewer-inner'
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        let oldParams = this.props.match.params;
        let newParams = nextProps.match.params;

        if(!this.animation && (oldParams.page !== newParams.page || oldParams.chapter !== newParams.chapter)) {
            this._distributeImages(this.state.pages, nextProps);
            return false
        }

        return true;
    }

    onTouch() {
        let current = this.props.images[this.state.position];
        if(current && !current.last) this._goForward();
    }

    onDragStart(e) {
        this.imageWidth = this.refs.viewer.clientWidth;
        this.dragX = e.clientX;
        this.setState({animate: false});
    }

    onDrag(e) {
        let dx = e.clientX - this.dragX;
        if(!((this.state.position === 0 && dx > 0) || (this.state.position === 4 && dx < 0))) {
            dx = Math.abs(dx) < this.imageWidth ? dx: this.imageWidth * Math.sign(dx);
            this.setState({dx: dx, animate: false});
        }
    }

    onDragEnd(e) {
        let dx = e.clientX - this.dragX, position = this.state.position, transition;
        dx = Math.abs(dx) < this.imageWidth ? dx: this.imageWidth * Math.sign(dx);
        if(!((this.state.position === 0 && dx > 0) || (this.state.position === 4 && dx < 0))) {

            let seeker = this.state.seeker;
            let direction = 0;

            let imageShift = 100 * Math.sign(dx) * dx / (5 * this.imageWidth);
            if (dx < 0) {
                direction = 1;
                imageShift = this.imageWidthInPercents - imageShift;
            }

            if (Math.abs(dx) > this.imageWidth / 2) {
                transition = -((position + direction) * this.imageWidthInPercents - imageShift);
                position = position - Math.sign(dx);
                seeker = seeker - Math.sign(dx);
            } else {
                transition = -((position + direction) * this.imageWidthInPercents - imageShift);
            }

            this.setState({dx: false, transition: transition, animate: false, position: position, seeker: seeker});
            setTimeout(() => {
                this.setState({transition: false, animate: true});
                this._startTimer();
            }, 10);

        } else {
            this.setState({dx: false, transition: false, animate: false})
        }

    }

    _startTimer() {
        this.animation = true;
        setTimeout(this._distributeImages.bind(this), 310);
    }

    _distributeImages(data, props) {
        this.animation = false;
        let position, seeker;
        let newState = {animate: false};

        if(data) {
            //initializing
            let {chapter, page} = props.match.params;
            page = Number(page);

            if(isNaN(page)) return;

            seeker = data.findIndex((image) => {
                return image.chapter === chapter && image.page === page;
            });

            position = 0;
            if(seeker < 2) {
                position = seeker
            } else if(seeker >= data.length - 1) {
                position = 5 - data.length + seeker
            } else {
                position = defaultPosition;
            }

            newState.pages = data;
        } else {

            data = this.state.pages;
            // position = this.state.position;
            seeker = this.state.seeker;

            let image, name = this.props.match.params.name;
            image = this.props.images[this.state.position];

            if(seeker < 2 || seeker > data.length - 3) {
                this._updateUrl(`#/comicbook/${name}/${image.chapter}/${image.page}`);
                return;
            }

            position = defaultPosition;

            this._updateUrl(`#/comicbook/${name}/${image.chapter}/${image.page}`);
        }

        let _images = this.props.images;
        let _firstImagePosition = seeker - position;

        _images.forEach((image, index) => {
            let _image = data[_firstImagePosition + index];
            if(_image) {
                image.url = `${URL}${_image.url}`;
                image.chapter = _image.chapter;
                image.page = _image.page;
                if(_image.first) image.first = true;
                if(_image.last) image.last = true;
            }
        });

        newState.transition = 0;
        newState.position = position;
        newState.seeker = seeker;

        this.setState(newState);
    }

    _updateUrl(url) {
        console.error(this.props);
        document.location.href = url;
    }

    _goForward() {
        this.setState({position: this.state.position + 1, animate: true, seeker: this.state.seeker + 1});
        this._startTimer();
    }

    _goBack() {
        this.setState({position: this.state.position - 1, animate: true, seeker: this.state.seeker - 1});
        this._startTimer();
    }

    _handleKeyDown(e) {
        if(!this.animation) {
            let current = this.props.images[this.state.position];
            if(current) {
                if(e.keyCode === 39 && !current.last) this._goForward();
                if(e.keyCode === 37 && !current.first) this._goBack();
            }
        }
    }

    render() {

        if(this.state.error === 404) return <div className="error">404</div>;

        const style = {
            transform   : `translate(-${this.state.position * 20}%)`,
            transition  : this.state.animate ? `0.3s ease-in-out`: ''
        };

        if(this.state.transition) {
            style.transform = `translate(${this.state.transition}%)`
        }

        if(this.state.dx) {
            style.left = this.state.dx + 'px'
        }

        return <div className="comicbook-viewer" ref="viewer">
            <div style={style} className="comicbook-viewer-inner" onClick={this.onTouch} onDrag={this.onDrag}
                 onDragEnd={this.onDragEnd} onDragStart={this.onDragStart}>
                {this.props.images.map(image =>
                    <ComicbookImage key={image.url} url={image.url} />
                )}
            </div>
        </div>
    }
}

ComicbookViewer.propTypes = {
    images  : PropTypes.arrayOf(PropTypes.shape(ComicbookImage.propTypes))
};

//generates default defaultImages data
let defaultImages = new Array(5);
for(let i = 0, l = defaultImages.length; i < l; ++i) {
    defaultImages[i] = JSON.parse(JSON.stringify(ComicbookImage.defaultProps));
    defaultImages[i].url = `none - ${i}`;
}

ComicbookViewer.defaultProps = {
    images  : defaultImages
};

export default ComicbookViewer;