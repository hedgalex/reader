import React, { Component } from 'react';
import PropTypes from "prop-types";

class ComicbookImage extends Component {

    render() {
        return (
            <img src={this.props.url} alt={this.props.id} title={this.props.id} />
        );
    }
}

ComicbookImage.propTypes = {
    url     : PropTypes.string.isRequired,
    chapter : PropTypes.string.isRequired,
    page    : PropTypes.number.isRequired,
    first   : PropTypes.bool,
    last    : PropTypes.bool,
};

ComicbookImage.defaultProps = {
    url     : '',
    chapter : '',
    page    : 0,
    first   : false,
    last    : false,
};

export default ComicbookImage;
