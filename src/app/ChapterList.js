import React, { Component } from 'react';
import {NavLink} from "react-router-dom";

const REST = 'http://hedgalex.nz:8001/comic';

class ChapterList extends Component {

    constructor(props) {
        super(props);

        let name = props.match.params.name;

        this.state = {
            chapterList : [],
            title       : name,
            name        : name
        }
    }

    getFirstPagePath(chapter, page) {
        return `${this.state.name}/${chapter}/${page}`;
    }

    componentDidMount() {
        fetch(`${REST}/list/${this.state.name}`)
            .then(results => {
                return results.json();
            })
            .then(data => {
                let {title, chapters, pageId} = data;
                let chapterList = chapters.map(chapter => {
                    return (
                        <li key={chapter} className="comicbook-list-item">
                            <NavLink to={this.getFirstPagePath(chapter, pageId)}>{title}: {chapter}</NavLink>
                        </li>
                    )
                });

                this.setState({title: title, chapterList: chapterList});
            })
    }

    render() {
        return (
            <div className="chapter-list">
                <h1>{this.state.title}</h1>
                <ul>{this.state.chapterList}</ul>
            </div>
        );
    }
}

export default ChapterList;
