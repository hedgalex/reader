import PropTypes from 'prop-types';

const ComicbookItem = () => {};

ComicbookItem.propTypes = {
    name    : PropTypes.string.isRequired,
    chapter : PropTypes.string.isRequired,
    page    : PropTypes.string.isRequired
};

export default ComicbookItem;