import React, { Component } from 'react';
import {HashRouter, Route} from "react-router-dom";
import './resources/app.css';

import ComicbookList from './ComicbookList';
import ChapterList from "./ChapterList";
import ComicbookViewer from './ComicbookViewer';
import {withCookies, Cookies} from "react-cookie";
import PropTypes from "prop-types";

class App extends Component {

    constructor(props) {
        super(props);

        // const { cookies } = props;
        // this.state = {
        //     url: cookies.get('url') || '/'
        // };
    }

    render() {
        return (
            <div className="app">
                <HashRouter>
                    <div className="content">
                        <Route exact path="/" component={ComicbookList} />
                        <Route exact path="/comicbook/:name" component={ChapterList} />
                        <Route path="/comicbook/:name/:chapter/:page" component={ComicbookViewer} />
                    </div>
                </HashRouter>
            </div>
        );
    }
}

App.propTypes = {
    cookies : PropTypes.shape(Cookies.default)
};

export default withCookies(App);
