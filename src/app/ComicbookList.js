import React, { Component } from 'react';
import {NavLink} from "react-router-dom";
import PropTypes from "prop-types";
import ComicbookItem from "./ComicbookItem";

const REST = 'http://hedgalex.nz:8001/comic';

class ComicbookList extends Component {

    constructor(props) {
        super(props);

        this.state = {
            comicList: []
        }
    }

    static getComicPath(name) {
        return `/comicbook/${name}`;
    }

    componentDidMount() {
        fetch(`${REST}/list`)
            .then(results => {
                return results.json();
            })
            .then(data => {
                let comicList = data.map(item => {
                    return (
                        <li className="comicbook-list-item" key={item.name}>
                            <NavLink to={ComicbookList.getComicPath(item.name)}>{item.title}</NavLink>
                        </li>
                    )
                });

                this.setState({comicList: comicList})
            })
    }

    render() {
        return (
            <ul className="comicbook-list">
                {this.state.comicList}
            </ul>
        );
    }
}

ComicbookList.propTypes = {
    name        : PropTypes.string.isRequired,
    title       : PropTypes.string.isRequired,
    comicList   : PropTypes.arrayOf(PropTypes.shape(ComicbookItem.propTypes))
};

export default ComicbookList;
